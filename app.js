/**
 * Module dependencies.
 */

var express = require('express');
var path = require('path');
var http = require('http');
var api = require('./api');
var routes = require('./routes');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.logger('dev'));

app.configure('development', function() {
  app.use(express.errorHandler({
    dumpExceptions: true,
    showStack: true
  }));
});

app.configure('production', function() {
  app.use(express.errorHandler());
});

// routes definitions

// widget control panel
app.get('/', routes.index);

// api calls
app.get('/weather', api.fulfillRequest, api.getWeather);
app.get('/weather.js', api.fulfillRequest, api.getWeatherWidget);
app.get('/config', api.getRunningConfig);
app.post('/config/city/:city', api.setCity);
app.post('/config/days/:days', api.setDaysToShow);
app.post('/config/layout/:layout', api.toggleLayout);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});