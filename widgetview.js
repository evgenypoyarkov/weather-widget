var mustache = require('mustache');
var redis = require('redis');
var path = require('path');
var fs = require('fs');

/* Module for widget rendering */

var getTemplate = function(name, cb) {
  var filename = path.join(__dirname + '/views/', name + '.mustache');
  fs.readFile(filename, 'utf-8', function(err, data) {
    if (err)
      throw err;

    console.log(data);
    cb(mustache.compile(data));
  });
};

function WidgetContext(city, layout, forecast) {
  this.city = city;
  this.layout = layout;
  this.forecast = forecast;
};

WidgetContext.prototype.date = function() {
  var month_names = {
    en: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 
         'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
  };
  var date = new Date(this.dt * 1000);
  return [
    "<div class='weather-widget-day-number'>",
      date.getDate(),
    "</div>",
    "<div class='weather-widget-month'>",
      month_names.en[date.getMonth()],
    "</div>"
  ].join('');
};

WidgetContext.prototype.decorate = function() {
  return function(t, render) {
    var v = Number(render(t)).toFixed();

    return ["<span class=", (v >= 0) ? "'warm'" : "'cold'", ">", v, "</span>"].join('');
  }
};

module.exports.render = function(rc, data, cb){
  console.log(arguments);
  var store = redis.createClient();
  var template = null;

  store.get(rc.viewhash, function(err, result) {
    if (err || !result) {
      getTemplate('widget', function(template){
        var context = new WidgetContext(rc.city, rc.layout, JSON.parse(data));
        var _html = template(context);

        cb(_html);

        store.set(rc.viewhash, _html);
        store.expire(rc.viewhash, 900);
      });
    }
    else {
      var _html = result;
      cb(_html);
    }
  });
};