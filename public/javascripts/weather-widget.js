(function(){
  var jQuery;

  if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.10.2') {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src", "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
        if (this.readyState == 'complete' || this.readyState == 'loaded') {
          scriptLoadHandler();
        }
      };
    } else { // Other browsers
      script_tag.onload = scriptLoadHandler;
    }
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);    
  } else {    
    jQuery = window.jQuery;
    main();
  }

  function scriptLoadHandler() {
    jQuery = window.jQuery.noConflict(true);
    main();
  }

  function main() {
    jQuery(document).ready(function($) {
    
      //setup vars
      var homeURL = '//127.0.0.1:3000/';
      
      var css_link = $("<link>", { 
        rel: "stylesheet", 
        type: "text/css", 
        href: homeURL + "stylesheets/widget.css" 
      });

      var $container = $('#weather-widget-container');

      //inject CSS required for widget
      css_link.appendTo('head');

      //an initial JSON request for data
      var url = [homeURL,'weather.js', '?callback=?'].join('');
      console.log(url);
      $.getJSON(url, function(data) {
        if (data.error)
          console.log('Cannot render the weather widget due to the error: ' + data.error);
        else
          $container.append(data.html);
      });
    });
  }
}());