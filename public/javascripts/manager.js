$(document).ready(function() {
  $('#cities').buttonset();
  $('#cities input').click(function(){
    console.log($(this).val());
    postData({ url: '/config/city/', data: $(this).val()});
    updateWidget(refreshWidget);
  });

  $('#days').buttonset();
  $('#days input').click(function(){
    console.log($(this).val());
    postData({ url: '/config/days/', data: $(this).val()});
    updateWidget(refreshWidget);
  });

  $('#layout').buttonset();
  $('#layout input').click(function(){
    console.log($(this).val());
    postData({ url: '/config/layout/', data: $(this).val()});
    updateWidget(refreshWidget);
  });

  var widgetContainer = $('#weather-widget-container');
  var widgetCodeContainer = $('#weather-widget-code');

  function postData(obj){
    if (typeof obj === 'object') {
      if (!obj.url){
        console.log('no url provided');
        return;
      }

      var url = obj.url + (obj.data ? obj.data : '');
      console.log(url);

      $.post(url, function(data){
        console.log(JSON.stringify(data));
      })
    }
  }

  (function initWidget(){
    updateWidget(refreshWidget);
  })();

  function updateWidget(cb){
    $.getJSON('/weather.js?callback=?', function(data){
      if (data.error) {
        console.log("Cannot render the weather widget due to the error: " + data.error);
      }

      if (data.html) {
        console.log(data.html);
        cb(data.html);
      }
    });
  }

  function refreshWidget(html){
    $(widgetContainer).html(html);
    $(widgetCodeContainer).html(escapeHtml(html));
  }

  /* Taken from mustache.js : */
  var entityMap = {                                                              
    "&": "&amp;",                                                                
    "<": "&lt;",                                                                 
    ">": "&gt;",                                                                 
    '"': '&quot;',                                                               
    "'": '&#39;',                                                                
    "/": '&#x2F;'                                                                
  };                                                                             
                                                                                  
  function escapeHtml(string) {                                                  
    return String(string).replace(/[&<>"'\/]/g, function (s) {                   
      return entityMap[s];                                                       
    });
  }
});