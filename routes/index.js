/* Weather widget controls
***************************/

var config = require('../runningConfig');

exports.index = function(req, res){
    var context = {
      title: 'Widget control page',
      rc: config.getRunningConfig()
    }
    res.render('index', context);
};