var url = require('url');
var querystring = require('querystring');
var crypto = require('crypto');

/* Object that stores configuration of the widget */
var config = {
  apikey: 'c0ea20c3024e2dffd6d111bcfad06102',

  cities:  [
    'Saint Petersburg',
    'Moscow',
    'Nizhny Novgorod'
    ],
  city:  'Moscow',
  daysToShow:  7,
  layout:  'h',
  hash: null,             // Hash for the data of this config
  viewhash: null,         // Hash for the template of the widget for this config
  url: null,              // URL of forecast data source
  lifetime: 900           // Lifetime of a record in cache (in seconds)
}

var setUrl = function() {
  var _query = querystring.stringify({
      q: config.city,
      mode: 'json',
      units: 'metric',
      cnt: config.daysToShow,
      appid: config.apikey
  });

  var _url = url.format({
      protocol: 'http',
      hostname: 'api.openweathermap.org',
      pathname: '/data/2.5/forecast/daily',
      search: _query
  });

  config.url = _url;
}

/* Set hash for the data */
var setHash = function() {
  var data = [config.city, config.daysToShow].join('');
  var hash = crypto.createHash('md5').update(data).digest('hex');

  config.hash = hash;
}

/* Set hash for the view */
var setViewHash = function() {
  var data = [config.city, config.daysToShow, config.layout].join('');
  var hash = crypto.createHash('md5').update(data).digest('hex');

  config.viewhash = hash;
  console.log(data, config.viewhash);
}

var updateConfig = function() {
  setUrl();
  setHash();
  setViewHash();
}

module.exports = {

  getRunningConfig: function() {
    if (!config)
      return null; // TODO: add error handling
    if (!config.hash || !config.url)
      updateConfig();
    return config;
  },

  setCity:  function(city) {
    if (!config)
      return; // TODO: add error handling

    var availableTypes = [Number, String];

    if (!city || availableTypes.indexOf(city.constructor) < 0){
      console.log ('bad data has been passed');
      return; // TODO: add error handling
    }

    if (city.constructor === Number) {
      if (config.cities[city])
        config.city = config.cities[city];
      else
        console.log ('no such city'); // TODO: add error handling
    } 
    else {
      if (config.cities.indexOf(city) !== -1)
        config.city = city;
      else
        console.log ('no such city'); // TODO: add error handling
    }
    updateConfig();
  },

  setDaysToShow:  function(nDays) {
    if (!config)
      return; // TODO: add error handling

    var nDays = Number(nDays);

    if (nDays && nDays.constructor === Number && [1, 3, 7].indexOf(nDays >= 0)) {
      config.daysToShow = nDays;
      updateConfig();
    }
    else
      console.log ('wrong value for the days to be shown has been passed'); // TODO: add error handling
  },

  toggleLayout:  function(layout) {
    if (!config)
      return; // TODO: add error handling

    if (layout === 'h' || layout === 'v')
      config.layout = layout;

    setViewHash();
  }
}