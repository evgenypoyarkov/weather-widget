var config = require('../runningConfig');

var request = require('request');
var redis = require('redis');
var mustache = require('mustache');
var widgetview = require('../widgetview');

/* middleware for request fulfilling by related forecast data */
module.exports.fulfillRequest = function(req, res, next) {
  var store = redis.createClient();
  var rc = config.getRunningConfig();

  store.get(rc.hash, function(err, result) {
    if (err || !result) {
      request.get(
        {
          uri: rc.url
        },
        function(err, response, body) {
          if (err || response.statusCode !== 200) {
            res.error = err.code || response.statusCode;
            next();
          }
          else {
            data = JSON.parse(body);

            if ('list' in data) {
              res.forecast = JSON.stringify(data.list);
              
              next();

              store.set(rc.hash, res.forecast);
              store.expire(rc.hash, rc.lifetime);
            }
          }
        }
      );
    }
    else {
      res.forecast = result;
      next();
    }
  });
}

/* Send forecast data in raw json. The data is based upon a running config. */
module.exports.getWeather = function(req, res) {
  console.log ('getWeather');

  res.send(200, res.forecast || 'no data now');  
}

/* Send callback-wrapped forecast data. The data is based upon a running config. */
module.exports.getWeatherWidget = function(req, res) {
  var rc = config.getRunningConfig();
  var cbdata = null;

  function cbresponce(cbdata) {
    res.header('Content-type', 'application/json');
    res.send(req.query.callback + '(' + JSON.stringify(cbdata) + ')')
  }

  if (res.error) {
    cbresponce({error: res.error});
  }
  else {
    if (req.query && req.query.callback) {
      widgetview.render(rc, res.forecast, function(html) {
        cbresponce({html: html});
      });
    }
    else {
      res.json({error: 'callback method has not been provided'});
    }
  }
}

/* Send the running config in json */
module.exports.getRunningConfig = function(req, res) {
  res.json(config.getRunningConfig());
}

/* Set a number of days that will be presented in widget */
module.exports.setDaysToShow = function(req, res) {
  console.log (req.params);

  if (req.params.days)
    config.setDaysToShow (req.params.days);

  res.send(200, 'ok');
}

/* Set city */
module.exports.setCity = function(req, res) {
  console.log(req.params);

  if (req.params.city)
    config.setCity(req.params.city);

  res.send(200, 'ok');
}

/* Toggle layout */
module.exports.toggleLayout = function(req, res) {
  console.log(req.params);

  if (req.params.layout)
    config.toggleLayout(req.params.layout);

  res.send(200, 'ok');
}